**Load Runner**

This tool helps to run a load on your API. The API can be either WebServices or RESTful. It accepts below parameters.

This tool will spin up number of threads based on the RPM which is provided and **does not accept any parameter as number of threads**

| parameters | Description | Is mandatory | Input format |
| :------: | :------: | :------: | :------: | 
| `-p`     | Path to the tests | Y | -p=Path containing request folder |
| `-r` | Requests per minute | Y | -r=10.2 |
| `-d` | Duration of the run in seconds | Y | -d=300 |

**Test**

How are the tests been organized to be invoked

Nomenclature will be alwasy put the file mentioned below in a folder request followed by number or any name viz., request1, request2, request3..

In each of the folders place files as 

*  request_metadata.json
*  request_postdata.txt
*  response_message.txt
*  response_metadata.json

**request_metadata.json**

`{
	"name":"test1",
	"url":"url of the api",
	"method":"POST",
	"headers":[{"key":"header key","value":"header value"}],
	"type":"rest"
}`
`{
	"name":"test2",
	"url":"url of the api",
	"method":"POST",
	"headers":[{"key":"header key","value":"header value"}],
	"type":"ws"
}`

**request_postdata.txt**

This containis just the post data that need to be given. If nothing is given then the post body will be ignored

**response_metadata.json**

`{
	"statuscode":200,
	"headers":[{"key":"header key","value":"header value"}],
	"assertions":["statuscode","responsemessage","headers"]
}`

In the above example, what ever is placed in assertions array, all those will be asserted in the request. If nothing is placed then the assertion will be ignored.
But if any of the assertion is failed, then execution will not be stopped

**response_message.txt**

Expected response message can be placed in this file.