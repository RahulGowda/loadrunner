package models;

import java.util.List;

@lombok.Data
public class response {
    private int statuscode;
    private List<keyvalue> headers;
    private String[] assertions;
    private String responsemessage;
}
