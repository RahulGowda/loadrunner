package main.java.handlers;

import models.request;
import models.response;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import static helpers.ConsoleColors.*;
import static helpers.Helper.isNullOrEmpty;
import static javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier;
import static javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory;

public abstract class handler {
    public boolean Invoke(request request, response response, String csvPath) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }};

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        setDefaultSSLSocketFactory(sc.getSocketFactory());
        javax.net.ssl.HostnameVerifier allHostsValid = (hostname, session) -> true;
        setDefaultHostnameVerifier(allHostsValid);

        org.apache.commons.lang.time.StopWatch st = new org.apache.commons.lang.time.StopWatch();
        st.start();

        URL obj = new URL(request.getUrl());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod(request.getMethod());
        for (models.keyvalue itr : request.getHeaders()) {
            con.setRequestProperty(itr.getKey(), itr.getValue());
        }

        if (!isNullOrEmpty(request.getPostdata())) {
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(request.getPostdata());
            wr.flush();
            wr.close();
        }
        String inputLine;
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
        } catch (Exception ex) {
            in = new BufferedReader(new InputStreamReader(
                    con.getErrorStream()));
        }

        StringBuffer resp = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            resp.append(inputLine);
        }
        in.close();

        boolean returnStatus = true;

        String[] assertions = response.getAssertions();
        if (assertions.length > 0) {
            for (String asert : assertions) {
                switch (asert) {
                    case "statuscode":
                        int status = response.getStatuscode();
                        returnStatus &= con.getResponseCode() == status;
                        PrintConsole(returnStatus, request.getName());
                        break;
                    case "responsemessage":
                        returnStatus &= resp.toString() == response.getResponsemessage();
                        PrintConsole(returnStatus, request.getName());
                        break;
                }
            }
        } else
            PrintConsole(true, request.getName());

        st.stop();
        long elapese = st.getTime();
        reports.Reporters.CsvWriter(csvPath, java.time.LocalDateTime.now().format(java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + "," + request.getUrl() + "," + returnStatus + "," + elapese, true);
        return returnStatus;
    }

    private void PrintConsole(boolean status, String message) {
        if (status)
            System.out.printf("%s: %sSUCCESS%s%n", message, GREEN, RESET);
        else
            System.out.printf("%s: %sFAIL%s%n", message, RED, RESET);
    }
}

