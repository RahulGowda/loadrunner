package Runner;

import models.request;
import models.response;

import java.util.HashMap;

import static helpers.ConsoleColors.RED;
import static helpers.ConsoleColors.RESET;
import static reports.Reporters.Report;

public class LoadRunner {
    static java.util.List<Thread> allThreads = new java.util.ArrayList<Thread>();

    public static void main(String[] args) throws org.apache.commons.cli.ParseException, java.io.IOException, InterruptedException {


        org.apache.commons.cli.Options options = new org.apache.commons.cli.Options();
        java.util.HashMap<request, response> requests
                = new HashMap<>();

        org.apache.commons.cli.Option tests = new org.apache.commons.cli.Option("p", "tests", true, "path to the tests location");
        tests.setRequired(true);
        options.addOption(tests);

        org.apache.commons.cli.Option rpmOption = new org.apache.commons.cli.Option("r", "rpm", true, "request per minute");
        rpmOption.setRequired(true);
        options.addOption(rpmOption);

        org.apache.commons.cli.Option totalDuration = new org.apache.commons.cli.Option("d", "duration", true, "duration in seconds");
        totalDuration.setRequired(true);
        options.addOption(totalDuration);

        org.apache.commons.cli.CommandLineParser parser = new org.apache.commons.cli.DefaultParser();
        org.apache.commons.cli.CommandLine cmd;
        cmd = parser.parse(options, args);


        String fileName = System.getProperty("user.dir") + "\\" + java.time.LocalDateTime.now().format(java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss")) + ".csv";
        Report(fileName);

        String testToBeExecuted = cmd.getOptionValue("p");
        java.io.File path = new java.io.File(testToBeExecuted);

        for (java.io.File p : java.util.Objects.requireNonNull(path.listFiles((x, name) -> x.isDirectory() && name.contains("request")))) {
            java.io.File[] files = p.listFiles((name) -> name.getName().equals("request_metadata.json"));
            java.io.InputStreamReader reader = new java.io.InputStreamReader(new java.io.FileInputStream(files != null ? files[0] : null));
            com.google.gson.Gson gson = new com.google.gson.Gson();
            request metaData = gson.fromJson(reader, request.class);
            files = p.listFiles((name) -> name.getName().contains("request_postdata"));
            metaData.setPostdata(new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(files != null ? files[0].getPath() : null))));

            files = p.listFiles((name) -> name.getName().equals("response_metadata.json"));
            reader = new java.io.InputStreamReader(new java.io.FileInputStream(files != null ? files[0] : null));
            gson = new com.google.gson.Gson();

            response rmetaData = gson.fromJson(reader, response.class);
            files = p.listFiles((name) -> name.getName().contains("response_message"));
            rmetaData.setResponsemessage(new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(files != null ? files[0].getPath() : null))));

            requests.put(metaData, rmetaData);
        }

        double rpm = Math.ceil(Double.valueOf(cmd.getOptionValue("r")));
        int rps = (int) Math.ceil(rpm / 60);

        long sleepDuration = 1000 / rps;
        int duration = Integer.parseInt(cmd.getOptionValue("d"));

        int totalPassed = 0, totalrequests = 0, totalfailed = 0;

        for (int i = 0; i < duration; i++) {
            for (int j = 0; j < rps; j++) {
                Object[] keys = requests.keySet().toArray();
                int randomIndex = new java.util.Random().nextInt(requests.size());
                response actResponse = requests.get(keys[randomIndex]);
                request actRequest = (request) keys[randomIndex];
                totalrequests++;
                boolean executionStatus = Invoker(actRequest, actResponse, fileName);
                if (executionStatus)
                    totalPassed++;
                else
                    totalfailed++;
            }
            Thread.sleep(sleepDuration);
        }

        for (int i = 0; i < allThreads.size(); i++) {
            if (allThreads.get(i).isAlive()) {
                System.out.println("Waiting for thread " + allThreads.get(i).getId() + " to close ");
                allThreads.get(i).join();
            }
        }

        System.out.println("---------------------------------------------------------------------");
        System.out.println("\033[0;1m" + "Summary of run");
        System.out.println("---------------------------------------------------------------------");
        System.out.println("|TotalRequests|Req per minute|Req per second|TotalPassed|TotalFailed|");
        System.out.printf("|%d           |%d            |%d            |%d         |%d         |%n",
                totalrequests, (int) rpm, rps, totalPassed, totalfailed);
        System.out.println("---------------------------------------------------------------------");

        System.out.println("Report generated at location " + fileName);

    }

    private static boolean Invoker(request req, response resp, String filePath) {
        java.util.concurrent.atomic.AtomicBoolean returnStatus = new java.util.concurrent.atomic.AtomicBoolean(true);
        Thread localthread;
        switch (req.getType()) {
            case "rest":
                localthread = new Thread(() -> {
                    try {
                        returnStatus.set(new main.java.handlers.restHandler().Invoke(req, resp, filePath));
                    } catch (Exception e) {
                        System.out.println(RED + e.getMessage() + RESET);
                    }
                });
                break;
            case "ws":
                localthread = new Thread(() -> {
                    try {
                        returnStatus.set(new main.java.handlers.wsHandler().Invoke(req, resp, filePath));
                    } catch (Exception e) {
                        System.out.println(RED + e.getMessage() + RESET);
                    }
                });
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + req.getType());
        }

        localthread.start();
        allThreads.add(localthread);

        return returnStatus.get();
    }
}
